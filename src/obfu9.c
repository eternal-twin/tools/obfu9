/*
 * Obfu9
 * Copyright (C) 2020-2022 Guillaume Charifi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>

#include "lib/hash.h"
#include "lib/opt.h"


static int print_usage(int argc, char **argv)
{
	const char *argv0 = argc >= 1 ? argv[0] : "obfu9";

	fprintf(stderr, "Usage:\n"
			"    %s                 [-k <key> | -hk <hashed_key>] [-t] [-m] [-o <output_file>]\n"
			"    %s -i <input_file> [-k <key> | -hk <hashed_key>] [-t] [-m] [-o <output_file>]\n"
			"    %s -s <value>      [-k <key> | -hk <hashed_key>] [-t] [-m] [-o <output_file>]\n"
			"\n"
			"Read either:\n"
			" - one identifier per line from stdin\n"
			" - one identifier per line from the specified <input_file>\n"
			" - specified <value> as a single identifier\n"
			"\n"
			"Hash either:\n"
			" - using default empty key \"\"\n"
			" - using specified <key>\n"
			" - using specified MD5 <hashed_key>\n"
			"\n"
			"Output for each input identifier either:\n"
			" - <hashed identifier><EOL> (default)\n"
			" - <hashed identifier><SEP><identifier><EOL> (with -m)\n"
			"\n"
			"Output format:\n"
			" - binary with <SEP>=<EOL>='\\0' (default)\n"
			" - plain text with <SEP>=' ' and <EOL>='\\n' (with -t)\n"
			"\n"
			"Output to either:\n"
			" - stdout\n"
			" - the specified <output_file>\n",
			argv0,
			argv0,
			argv0);
	return 1;
}

static inline int hexdigit(int n) {
	if (n >= 0x0 && n <= 0x9)
		return '0' + n - 0x0;
	if (n >= 0xA && n <= 0xF)
		return 'A' + n - 0xA;
	return '?';
}

static void compute_and_output_hash(struct obfu9_hash_context *ctx, FILE *out_fp, const char *str, bool is_plaintext, bool is_mapping)
{
	size_t str_len;
	size_t str_sz;
	char buf[OBFU9_HASH_SIZE];
	size_t buf_len;
	size_t buf_sz;
	char *p;

	str_len = strlen(str);
	str_sz = str_len + 1;
	obfu9_hash_obfuscate_unpacked(ctx, buf, str, str_len);
	buf_len = strlen(buf);
	buf_sz = buf_len + 1;

	if (!is_plaintext)
	{
		fwrite(buf, buf_sz, 1, out_fp);
		if (is_mapping)
			fwrite(str, str_sz, 1, out_fp);
		return;
	}

	p = buf;
	while (*p)
	{
		char buf[4];
		size_t buf_len = 0;
		unsigned char c = (unsigned char)*p;

		if (c == '\\')
		{
			buf[0] = '\\';
			buf[1] = '\\';
			buf_len = 2;
		}
		else if (c >= 0x21 && c <= 0x7E)
		{
			buf[0] = c;
			buf_len = 1;
		}
		else
		{
			buf[0] = '\\';
			buf[1] = 'x';
			buf[2] = hexdigit((c >> 4) & 0xF);
			buf[3] = hexdigit(c & 0xF);
			buf_len = 4;
		}

		fwrite(buf, buf_len, 1, out_fp);
		p++;
	}

	if (is_mapping)
	{
		fputc(' ', out_fp);
		fwrite(str, str_len, 1, out_fp);
	}

	fputc('\n', out_fp);
}

enum opt_id
{
	OPT_HK = 0,
	OPT_I,
	OPT_K,
	OPT_M,
	OPT_O,
	OPT_S,
	OPT_T,
	OPT__END_
};

static inline int ctox(char c) {
	if (c >= '0' && c <= '9')
		return c - '0';
	if (c >= 'A' && c <= 'F')
		return 0xA + c - 'A';
	if (c >= 'a' && c <= 'f')
		return 0xA + c - 'a';
	return INT_MAX;
}

int main(int argc, char **argv)
{
	int status = 1;
	struct obfu9_opt opt_tpl[] = {
		OBFU9_OPT_STR(OPT_HK, "-hk"),
		OBFU9_OPT_STR(OPT_I,  "-i"),
		OBFU9_OPT_STR(OPT_K,  "-k"),
		OBFU9_OPT_NONE(OPT_M,  "-m"),
		OBFU9_OPT_STR(OPT_O,  "-o"),
		OBFU9_OPT_STR(OPT_S,  "-s"),
		OBFU9_OPT_NONE(OPT_T,  "-t"),
		OBFU9_OPT_END(OPT__END_)
	};
	struct obfu9_opt *opts;
	struct obfu9_hash_context ctx;
	const char *in_filename;
	const char *hk;
	const char *key;
	bool is_mapping;
	const char *out_filename;
	const char *in_value;
	bool is_plaintext;
	FILE *in_fp = stdin;
	FILE *out_fp = stdout;

	/*** Handle options ***/

	opts = obfu9_opt_parse(argc, argv, opt_tpl);
	if (!opts)
		return print_usage(argc, argv);

	if (obfu9_opt_get_defined(opts, OPT_HK) && obfu9_opt_get_defined(opts, OPT_K))
	{
		fprintf(stderr, "*ERR*: Only one of the options -hk or -k can be given at a time.\n\n");
		return print_usage(argc, argv);
	}

	if (obfu9_opt_get_defined(opts, OPT_I) && obfu9_opt_get_defined(opts, OPT_S))
	{
		fprintf(stderr, "*ERR*: Only one of the options -i or -s can be given at a time.\n\n");
		return print_usage(argc, argv);
	}

	in_filename  = obfu9_opt_get_str_or(opts, OPT_I, NULL);
	hk           = obfu9_opt_get_str_or(opts, OPT_HK, NULL);
	key          = obfu9_opt_get_str_or(opts, OPT_K, "");
	is_mapping   = obfu9_opt_get_defined(opts, OPT_M);
	out_filename = obfu9_opt_get_str_or(opts, OPT_O, NULL);
	in_value     = obfu9_opt_get_str_or(opts, OPT_S, NULL);
	is_plaintext = obfu9_opt_get_defined(opts, OPT_T);

	if (hk)
	{
		size_t hk_len;

		hk_len = strlen(hk);
		if (hk_len != OBFU9_HKEY_LEN * 2)
		{
			fprintf(stderr, "*ERR*: Invalid hkey supplied: length %zu != expected %d.\n\n", hk_len, OBFU9_HKEY_LEN * 2);
			return print_usage(argc, argv);
		}

		for (size_t i = 0; i < hk_len; i++)
		{
			if (ctox(hk[i]) != INT_MAX)
				continue;

			fprintf(stderr, "*ERR*: Invalid hkey supplied: invalid hex char '%c' at index %zu.\n\n", hk[i], i);
			return print_usage(argc, argv);
		}
	}

	/*** Open files ***/

	if (in_filename)
	{
#ifdef EISDIR
		errno = 0;
		in_fp = fopen(in_filename, "r+");
		if (in_fp)
			fclose(in_fp);

		if (errno != EISDIR)
		{
#endif
			errno = 0;
			in_fp = fopen(in_filename, "r");
#ifdef EISDIR
		}
#endif

		if (!in_fp)
		{
			fprintf(stderr, "*ERR*: Failed to open the input file \"%s\": %s\n", in_filename, strerror(errno));
			goto fail_open_in_fp;
		}
	}

	if (out_filename)
	{
		errno = 0;
		out_fp = fopen(out_filename, "wb");
		if (!out_fp)
		{
			fprintf(stderr, "*ERR*: Failed to open the output file \"%s\": %s\n", out_filename, strerror(errno));
			goto fail_open_out_fp;
		}
	}

	/*** Compute hashes ***/

	if (hk)
	{
		uint8_t hkey[OBFU9_HKEY_LEN];

		for (size_t i = 0; i < OBFU9_HKEY_LEN; i++)
			hkey[i] = (ctox(hk[i * 2]) << 4) | ctox(hk[i * 2 + 1]);

		obfu9_hash_context_init_raw(&ctx, hkey);
	}
	else
		obfu9_hash_context_init(&ctx, key, strlen(key));

	if (in_value)
		compute_and_output_hash(&ctx, out_fp, in_value, is_plaintext, is_mapping);
	else
	{
		while (!feof(in_fp) && !ferror(in_fp))
		{
			static char line_buf[1024];
			char *s;

			s = fgets(line_buf, sizeof(line_buf), in_fp);
			if (!s || line_buf[0] == '\n')
				continue;

			/* Remove the trailing '\n' */
			line_buf[strcspn(line_buf, "\n")] = '\0';

			compute_and_output_hash(&ctx, out_fp, line_buf, is_plaintext, is_mapping);
		}

		if (ferror(in_fp))
			goto fail_read_in_fp;
	}

	status = 0;

fail_read_in_fp:
	if (out_filename)
		fclose(out_fp);

fail_open_out_fp:
	if (in_filename)
		fclose(in_fp);

fail_open_in_fp:
	return status;
}
