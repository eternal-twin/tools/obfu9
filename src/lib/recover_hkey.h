/*
 * Obfu9
 * Copyright (C) 2020-2022 Guillaume Charifi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OBFU9_RECOVER_HKEY_H
#define OBFU9_RECOVER_HKEY_H 1

#include <stddef.h>
#include <stdint.h>

#include "hash.h"
#include "hkey.h"

enum obfu9_recover_hkey_log_level
{
	OBFU9_RECOVER_HKEY_LOG_LEVEL_SILENT = 0,
	OBFU9_RECOVER_HKEY_LOG_LEVEL_CRITICAL = 1,
	OBFU9_RECOVER_HKEY_LOG_LEVEL_ERROR = 2,
	OBFU9_RECOVER_HKEY_LOG_LEVEL_WARN = 3,
	OBFU9_RECOVER_HKEY_LOG_LEVEL_INFO = 4,
	OBFU9_RECOVER_HKEY_LOG_LEVEL_DEBUG = 5,
	OBFU9_RECOVER_HKEY_LOG_LEVEL_TRACE = 6
};

enum obfu9_recover_hkey_errno
{
	OBFU9_RECOVER_HKEY_ESUCCESS = 0,
	OBFU9_RECOVER_HKEY_ECONFLICT = -1,
	OBFU9_RECOVER_HKEY_EDUPLICATE = -2,
	OBFU9_RECOVER_HKEY_EFAIL = -3,
	OBFU9_RECOVER_HKEY_EINVAL = -4,
	OBFU9_RECOVER_HKEY_ENOENOUGHENT = -5,
	OBFU9_RECOVER_HKEY_ENOMEM = -6,
	OBFU9_RECOVER_HKEY_ENOTFOUND = -7,
	OBFU9_RECOVER_HKEY_EOVERFLOW = -8
};

enum obfu9_recover_hkey_stage
{
	OBFU9_RECOVER_HKEY_STAGE_LOAD = 0,
	OBFU9_RECOVER_HKEY_STAGE_EXTRACT_1_4 = 1,
	OBFU9_RECOVER_HKEY_STAGE_EXTRACT_ABOVE_4 = 2,
	OBFU9_RECOVER_HKEY_STAGE_BRUTEFORCE = 3,

	OBFU9_RECOVER_HKEY_STAGE_MAX = 4,
	OBFU9_RECOVER_HKEY_STAGE_INVAL = OBFU9_RECOVER_HKEY_STAGE_MAX
};

enum obfu9_recover_hkey_sub_stage
{
	OBFU9_RECOVER_HKEY_SUB_STAGE_SCHEDULER = 0,
	OBFU9_RECOVER_HKEY_SUB_STAGE_MAIN = 1,
	OBFU9_RECOVER_HKEY_SUB_STAGE_SHUFFLE = 2,

	OBFU9_RECOVER_HKEY_SUB_STAGE_MAX = 3,
	OBFU9_RECOVER_HKEY_SUB_STAGE_INVAL = OBFU9_RECOVER_HKEY_SUB_STAGE_MAX
};

enum obfu9_recover_hkey_step
{
	OBFU9_RECOVER_HKEY_STEP_KN_ABOVE_7F = 0,
	OBFU9_RECOVER_HKEY_STEP_OFFSET = 1,
	OBFU9_RECOVER_HKEY_STEP_MAIN = 2,
	OBFU9_RECOVER_HKEY_STEP_COUNT_MISSING_ENTRIES = 3,
	OBFU9_RECOVER_HKEY_STEP_KXK_FROM_PXK = 4,
	OBFU9_RECOVER_HKEY_STEP_KXK_FROM_KXK = 5,
	OBFU9_RECOVER_HKEY_STEP_PXK_FROM_PXK_X_KXK = 6,
	OBFU9_RECOVER_HKEY_STEP_PXP_FROM_PXK = 7,
	OBFU9_RECOVER_HKEY_STEP_LOG_COMPLETION = 8,
	OBFU9_RECOVER_HKEY_STEP_CHECK_COMPLETE_KXK = 9,

	OBFU9_RECOVER_HKEY_STEP_MAX = 10,
	OBFU9_RECOVER_HKEY_STEP_INVAL = OBFU9_RECOVER_HKEY_STEP_MAX
};

enum obfu9_recover_hkey_match_type
{
	OBFU9_RECOVER_HKEY_MATCH_TYPE_MATCH = 0,
	OBFU9_RECOVER_HKEY_MATCH_TYPE_MARKER_EXTRACT_1_4 = 1,
	OBFU9_RECOVER_HKEY_MATCH_TYPE_MARKER_EXTRACT_ABOVE_4 = 2,
	OBFU9_RECOVER_HKEY_MATCH_TYPE_MARKER_BRUTEFORCE = 3,
	OBFU9_RECOVER_HKEY_MATCH_TYPE_MARKER_SHUFFLE = 4
};

struct obfu9_recover_hkey_context;

typedef void (*obfu9_recover_hkey_log_cb_t)(void *priv, struct obfu9_recover_hkey_context *ctx, enum obfu9_recover_hkey_log_level lvl, const char *format, ...);

struct obfu9_recover_hkey_match
{
	enum obfu9_recover_hkey_match_type type;
	char *identifier;
	obfu9_hash_packed_t hash;
};

struct obfu9_recover_hkey_context
{
	int16_t kxk[OBFU9_HKEY_LEN][OBFU9_HKEY_LEN];
	int16_t pxk[OBFU9_HKEY_PERMUT_LEN][OBFU9_HKEY_LEN];
	int16_t pxp[OBFU9_HKEY_PERMUT_LEN][OBFU9_HKEY_PERMUT_LEN];
	char kn_above_7F[OBFU9_HKEY_LEN];
	char hkey[OBFU9_HKEY_LEN];
	enum obfu9_recover_hkey_log_level log_lvls[OBFU9_RECOVER_HKEY_STAGE_MAX][OBFU9_RECOVER_HKEY_SUB_STAGE_MAX][OBFU9_RECOVER_HKEY_STEP_MAX];
	obfu9_recover_hkey_log_cb_t log_cb;
	void *log_cb_priv;
	enum obfu9_recover_hkey_stage stage;
	enum obfu9_recover_hkey_sub_stage sub_stage;
	enum obfu9_recover_hkey_step step;
	size_t match_count;
	struct obfu9_recover_hkey_match *matches;
	size_t stage_iteration_index;
	int has_data_changed;
};

#ifdef __cplusplus
extern "C" {
#endif

extern void obfu9_recover_hkey_context_reset(struct obfu9_recover_hkey_context *ctx);
extern void obfu9_recover_hkey_context_init(struct obfu9_recover_hkey_context *ctx);
extern void obfu9_recover_hkey_set_log_callback(struct obfu9_recover_hkey_context *ctx, obfu9_recover_hkey_log_cb_t cb, void *cb_priv);
extern enum obfu9_recover_hkey_errno obfu9_recover_hkey_append_match(struct obfu9_recover_hkey_context *ctx, const char *identifier, obfu9_hash_packed_t h);
extern enum obfu9_recover_hkey_errno obfu9_recover_hkey_append_marker_extract_1_4(struct obfu9_recover_hkey_context *ctx);
extern enum obfu9_recover_hkey_errno obfu9_recover_hkey_append_marker_extract_above_4(struct obfu9_recover_hkey_context *ctx);
extern enum obfu9_recover_hkey_errno obfu9_recover_hkey_append_marker_bruteforce(struct obfu9_recover_hkey_context *ctx);
extern enum obfu9_recover_hkey_errno obfu9_recover_hkey_append_marker_shuffle(struct obfu9_recover_hkey_context *ctx);
extern enum obfu9_recover_hkey_errno obfu9_recover_hkey_run(struct obfu9_recover_hkey_context *ctx);

#ifdef __cplusplus
}
#endif

#endif /* OBFU9_RECOVER_HKEY_H */
