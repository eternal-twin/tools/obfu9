/*
 * Obfu9
 * Copyright (C) 2020-2022 Guillaume Charifi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <limits.h>
#include <stddef.h>
#include <stdint.h>

#include "hkey.h"

#include "hash.h"

static inline int unpack_hash(char buf[OBFU9_HASH_SIZE], obfu9_hash_packed_t src_hash) {
	while (src_hash != 0)
	{
		*buf++ = src_hash & OBFU9_HASH_PACKED_CHAR_MASK;

		src_hash >>= OBFU9_HASH_PACKED_CHAR_BIT;
	}

	*buf = '\0';
	return 0;
}

static inline obfu9_hash_packed_t pack_hash(const char buf[OBFU9_HASH_SIZE]) {
	obfu9_hash_packed_t h = 0;
	size_t shift = 0;

	while (*buf)
	{
		h |= *buf++ << shift;

		shift += OBFU9_HASH_PACKED_CHAR_BIT;
	}

	return h;
}

static inline obfu9_hash_packed_t pack_hash_len(const char buf[OBFU9_HASH_SIZE], size_t len) {
	obfu9_hash_packed_t h = 0;
	size_t shift = 0;

	while (len--)
	{
		h |= *buf++ << shift;

		shift += OBFU9_HASH_PACKED_CHAR_BIT;
	}

	return h;
}

static inline obfu9_hash_packed_t map_char(obfu9_hash_packed_t c) {
	/*
	 * '\0' 0x00 => '\x01' 0x01
	 * '<'  0x3C => '('    0x28
	 * '>'  0x3E => ')'    0x29
	 * '"'  0x22 => '\''   0x27
	 * '.'  0x2E => ';'    0x3B
	 * ':'  0x3A => ';'    0x3B
	 */
	static const uint8_t m[1 << OBFU9_HASH_PACKED_CHAR_BIT] = {
		0x01, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
		0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F,
		0x20, 0x21, 0x27, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2A, 0x2B, 0x2C, 0x2D, 0x3B, 0x2F,
		0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3B, 0x3B, 0x28, 0x3D, 0x29, 0x3F,
		0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4A, 0x4B, 0x4C, 0x4D, 0x4E, 0x4F,
		0x50, 0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59, 0x5A, 0x5B, 0x5C, 0x5D, 0x5E, 0x5F,
		0x60, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69, 0x6A, 0x6B, 0x6C, 0x6D, 0x6E, 0x6F,
		0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79, 0x7A, 0x7B, 0x7C, 0x7D, 0x7E, 0x7F
	};

	return m[c & OBFU9_HASH_PACKED_CHAR_MASK];
}

static inline obfu9_hash_packed_t packed_hash_fixup(obfu9_hash_packed_t hash) {
	obfu9_hash_packed_t h = 0;
	size_t shift = 0;

	do
	{
		obfu9_hash_packed_t hl7 = hash & OBFU9_HASH_PACKED_CHAR_MASK;

		hl7 = map_char(hl7);
		h |= hl7 << shift;

		shift += OBFU9_HASH_PACKED_CHAR_BIT;
		hash >>= OBFU9_HASH_PACKED_CHAR_BIT;
	}
	while (hash != 0);

	return h;
}

static inline obfu9_hash_packed_t run_raw(struct obfu9_hash_context *ctx, const char *identifier, size_t len) {
	const uint8_t *permut = ctx->hkey.permut;
	const uint8_t *hkey = ctx->hkey.hkey;
	const char* endp;
	obfu9_hash_packed_t offset = 0;
	obfu9_hash_packed_t h = 0;

	if (len < 1)
		return 0;

	/* Unroll the loop for performance */
	endp = &identifier[len - 1];
	for (const char *id = identifier; id < endp;)
	{
		obfu9_hash_packed_t a = *id++;
		obfu9_hash_packed_t b = *id++;

		offset = offset * 0x83 * 0x83 + a * 0x83 + b;
	}

	if (len & 1)
		offset = offset * 0x83 + *endp;

	offset &= UINT32_C(0x7FFFFFFF);

	/* Unroll the loop for performance */
	for (size_t ii = 0; ii < len - 1; ii += 2)
	{
		size_t i = len - 1 - ii;

		obfu9_hash_packed_t a = permut[(identifier[i] + offset) & 0x7F] ^ hkey[i & 0xF];
		i--;
		obfu9_hash_packed_t b = permut[(identifier[i] + offset) & 0x7F] ^ hkey[i & 0xF];

		h = h * 0x83 * 0x83 + a * 0x83 + b;
	}

	if (len & 1)
		h = h * 0x83 + (permut[(identifier[0] + offset) & 0x7F] ^ hkey[0]);

	/* h = abs(h) */
	if (h & UINT32_C(0x40000000))
		h = ~(h - 1);
	h &= UINT32_C(0x3FFFFFFF);

	return h;
}

static inline obfu9_hash_packed_t run_packed(struct obfu9_hash_context *ctx, const char *identifier, size_t len) {
	obfu9_hash_packed_t h = 0;

	h = run_raw(ctx, identifier, len);

	return packed_hash_fixup(h);
}

int obfu9_hash_context_init_raw(struct obfu9_hash_context *ctx, uint8_t hkey[OBFU9_HKEY_LEN])
{
	int result;

	result = obfu9_hkey_gen_raw(&ctx->hkey, hkey);
	if (result < 0)
		return result;

	return 0;
}

int obfu9_hash_context_init(struct obfu9_hash_context *ctx, const char *key, size_t key_len)
{
	int result;

	result = obfu9_hkey_gen(&ctx->hkey, key, key_len);
	if (result < 0)
		return result;

	return 0;
}

void obfu9_hash_unpack(char buf[OBFU9_HASH_SIZE], obfu9_hash_packed_t src_hash)
{
	unpack_hash(buf, src_hash);
}

obfu9_hash_packed_t obfu9_hash_pack(const char buf[OBFU9_HASH_SIZE])
{
	return pack_hash(buf);
}

obfu9_hash_packed_t obfu9_hash_pack_len(const char buf[OBFU9_HASH_SIZE], size_t len)
{
	return pack_hash_len(buf, len);
}

obfu9_hash_packed_t obfu9_hash_packed_fixup(obfu9_hash_packed_t hash)
{
	return packed_hash_fixup(hash);
}

obfu9_hash_packed_t obfu9_hash_obfuscate_unfixed(struct obfu9_hash_context *ctx, const char *identifier, size_t len)
{
	return run_raw(ctx, identifier, len);
}

obfu9_hash_packed_t obfu9_hash_obfuscate(struct obfu9_hash_context *ctx, const char *identifier, size_t len)
{
	return run_packed(ctx, identifier, len);
}

void obfu9_hash_obfuscate_unpacked(struct obfu9_hash_context *ctx, char buf[OBFU9_HASH_SIZE], const char *identifier, size_t len)
{
	obfu9_hash_packed_t h;

	h = run_packed(ctx, identifier, len);
	unpack_hash(buf, h);
}

size_t obfu9_hash_enumerate_unfixed_count(obfu9_hash_packed_t hash)
{
	size_t n = 1;

	while (hash)
	{
		uint8_t hl7 = hash & OBFU9_HASH_PACKED_CHAR_MASK;

		switch (hl7)
		{
			case '\x01': n *= (hash >> OBFU9_HASH_PACKED_CHAR_BIT) ? 2 : 1; break;
			case '(':    n *= 2; break;
			case ')':    n *= 2; break;
			case '\'':   n *= 2; break;
			case ';':    n *= 3; break;
		}

		hash >>= OBFU9_HASH_PACKED_CHAR_BIT;
	}

	return n;
}

static inline obfu9_hash_packed_t replace_packed_char_at(obfu9_hash_packed_t hash, uint8_t shift, uint8_t val)
{
	hash &= ~(OBFU9_HASH_PACKED_CHAR_MASK << shift);
	hash |= val << shift;
	return hash;
}

/* Recursive function to enumerate all variations on a packed hash */
static void enumerate_raw_inner(obfu9_hash_packed_t **hash_buf, obfu9_hash_packed_t cur_hash, uint8_t shift)
{
	uint8_t next_shift = shift + OBFU9_HASH_PACKED_CHAR_BIT;
	uint8_t cur_char = (cur_hash >> shift) & OBFU9_HASH_PACKED_CHAR_MASK;

	if (shift >= sizeof(cur_hash) * CHAR_BIT)
	{
		/* Recursion done, write the hash and exit */
		**hash_buf = cur_hash;
		(*hash_buf)++;
		return;
	}

	/* Recurse on all variations of the next packed char */
	enumerate_raw_inner(hash_buf, cur_hash, next_shift);

	switch (cur_char)
	{
		case '\x01':
		{
			obfu9_hash_packed_t replaced = replace_packed_char_at(cur_hash, shift, '\0');
			if ((replaced >> shift) != 0)
				enumerate_raw_inner(hash_buf, replaced, next_shift);
			break;
		}

		case '(':
			enumerate_raw_inner(hash_buf, replace_packed_char_at(cur_hash, shift, '<'), next_shift);
			break;

		case ')':
			enumerate_raw_inner(hash_buf, replace_packed_char_at(cur_hash, shift, '>'), next_shift);
			break;

		case '\'':
			enumerate_raw_inner(hash_buf, replace_packed_char_at(cur_hash, shift, '"'), next_shift);
			break;

		case ';':
			enumerate_raw_inner(hash_buf, replace_packed_char_at(cur_hash, shift, '.'), next_shift);
			enumerate_raw_inner(hash_buf, replace_packed_char_at(cur_hash, shift, ':'), next_shift);
			break;
	}
}

obfu9_hash_packed_t *obfu9_hash_enumerate_unfixed(obfu9_hash_packed_t *buf, obfu9_hash_packed_t hash)
{	
	enumerate_raw_inner(&buf, hash, 0);
	return buf;
}
