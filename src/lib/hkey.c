/*
 * Obfu9
 * Copyright (C) 2020-2022 Guillaume Charifi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include "macro.h"
#include "md5.h"

#include "hkey.h"

static int obfu9_hkey_gen_common(struct obfu9_hkey_context *ctx)
{
	int result;

	result = obfu9_prng_init(&ctx->prng, ctx->hkey, OBFU9_HKEY_LEN);
	if (result < 0)
		return result;

	for (size_t i = 0; i < OBFU9_HKEY_PERMUT_LEN; i++)
		ctx->permut[i] = i;

	for (size_t i = 1; i <= 500; i++)
	{
		size_t ipos = i & 0x7F;
		size_t pos = obfu9_prng_next(&ctx->prng, 0x80);
		uint8_t k = ctx->permut[pos];

		ctx->permut[pos] = ctx->permut[ipos];
		ctx->permut[ipos] = k;
	}

	return 0;
}

int obfu9_hkey_gen_raw(struct obfu9_hkey_context *ctx, uint8_t hkey[OBFU9_HKEY_LEN])
{
	ctx->key = NULL;
	ctx->len = 0;
	memcpy(ctx->hkey, hkey, sizeof(ctx->hkey));

	return obfu9_hkey_gen_common(ctx);
}

int obfu9_hkey_gen(struct obfu9_hkey_context *ctx, const char *key, size_t len)
{
	static const char part0[] = "salt";
	static const char part2[] = "_salt_";
	int result;

	ctx->key = key;
	ctx->len = len;
	result = obfu9_md5_multipart(ctx->hkey,
			part0, OBFU9_STRLEN(part0),
			ctx->key, ctx->len,
			part2, OBFU9_STRLEN(part2),
			ctx->key, ctx->len,
			NULL);
	if (result < 0)
		return result;

	return obfu9_hkey_gen_common(ctx);
}
