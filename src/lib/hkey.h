/*
 * Obfu9
 * Copyright (C) 2020-2022 Guillaume Charifi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OBFU9_HKEY_H
#define OBFU9_HKEY_H 1

#include <stddef.h>
#include <stdint.h>

#include "md5.h"
#include "prng.h"

#define OBFU9_HKEY_LEN OBFU9_MD5_HASH_SIZE
#define OBFU9_HKEY_PERMUT_LEN 128

struct obfu9_hkey_context
{
	const char *key;
	size_t len;
	uint8_t hkey[OBFU9_HKEY_LEN];
	struct obfu9_prng_context prng;
	uint8_t permut[OBFU9_HKEY_PERMUT_LEN];
};

#ifdef __cplusplus
extern "C" {
#endif

extern int obfu9_hkey_gen_raw(struct obfu9_hkey_context *ctx, uint8_t hkey[OBFU9_HKEY_LEN]);
extern int obfu9_hkey_gen(struct obfu9_hkey_context *ctx, const char *key, size_t len);

#ifdef __cplusplus
}
#endif

#endif /* OBFU9_HKEY_H */
