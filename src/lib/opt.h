/*
 * Obfu9
 * Copyright (C) 2020-2022 Guillaume Charifi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OBFU9_OPT_H
#define OBFU9_OPT_H 1

#include <stddef.h>

#define OBFU9_OPT_NONE(e, n) [e] = { .type = OBFU9_OPT_TYPE_NONE, .name = n }
#define OBFU9_OPT_STR(e, n) [e] = { .type = OBFU9_OPT_TYPE_STR, .name = n }
#define OBFU9_OPT_LONG(e, n) [e] = { .type = OBFU9_OPT_TYPE_LONG, .name = n }
#define OBFU9_OPT_END(e) [e] = { .type = OBFU9_OPT_TYPE_INVAL, .name = NULL }

enum obfu9_opt_type
{
	OBFU9_OPT_TYPE_INVAL = 0,
	OBFU9_OPT_TYPE_NONE,
	OBFU9_OPT_TYPE_LONG,
	OBFU9_OPT_TYPE_STR
};

struct obfu9_opt
{
	enum obfu9_opt_type type;
	const char *name;
	unsigned int defined : 1;
	union {
		long as_long;
		const char *as_str;
	} data;
};

#ifdef __cplusplus
extern "C" {
#endif

extern struct obfu9_opt *obfu9_opt_parse(int argc, char **argv, struct obfu9_opt *tpl);
extern int obfu9_opt_get_defined(const struct obfu9_opt *opts, size_t id);
extern long obfu9_opt_get_long(const struct obfu9_opt *opts, size_t id);
extern long obfu9_opt_get_long_or(const struct obfu9_opt *opts, size_t id, long default_val);
extern const char *obfu9_opt_get_str(const struct obfu9_opt *opts, size_t id);
extern const char *obfu9_opt_get_str_or(const struct obfu9_opt *opts, size_t id, const char *default_val);

#ifdef __cplusplus
}
#endif

#endif /* OBFU9_OPT_H */
