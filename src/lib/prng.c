/*
 * Obfu9
 * Copyright (C) 2020-2022 Guillaume Charifi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "md5.h"

#include "prng.h"

static inline size_t max2_size_t(size_t a, size_t b) {
	return a > b ? a : b;
}

int obfu9_prng_init(struct obfu9_prng_context *ctx, const void *seed, size_t seed_len)
{
	const uint8_t *sd = (const uint8_t *)seed;
	char accu[OBFU9_MD5_HASH_SIZE] = "x";
	size_t accu_len = 1;
	size_t limit;

	for (size_t i = 0; i < OBFU9_PRNG_ARRAY_LEN; i++)
		ctx->data[i] = i;

	limit = OBFU9_PRNG_ARRAY_LEN + max2_size_t(OBFU9_PRNG_ARRAY_LEN, seed_len);
	for (size_t i = 0; i < limit; i++)
	{
		char seed_buf[4]; /* max = "255\0" */
		size_t j = i % OBFU9_PRNG_ARRAY_LEN;
		size_t k = i % seed_len;

		snprintf(seed_buf, sizeof(seed_buf), "%" PRIu8, sd[k]);
		obfu9_md5_multipart(accu,
			accu, accu_len,
			seed_buf, strlen(seed_buf),
			NULL);

		accu_len = OBFU9_MD5_HASH_SIZE;

		ctx->data[j] ^= ((accu[0] + (accu[1] << 8) + (accu[2] << 16))) ^ (accu[3] << 22);
	}

	ctx->idx = 0;
	return 0;
}

static inline uint32_t obfu9_prng_next_bits(struct obfu9_prng_context *ctx) {
	ctx->idx = (ctx->idx + 1) % OBFU9_PRNG_ARRAY_LEN;

	ctx->data[ctx->idx] = (ctx->data[(ctx->idx + 24) % OBFU9_PRNG_ARRAY_LEN]
			+ ctx->data[ctx->idx]) & 0x3FFFFFFF;

	return ctx->data[ctx->idx];
}

uint32_t obfu9_prng_next(struct obfu9_prng_context *ctx, size_t limit)
{
	uint32_t r;
	uint32_t v;

	do
	{
		r = obfu9_prng_next_bits(ctx);
		v = r % limit;
	}
	while (r - v > 0x3FFFFFFF - limit + 1);

	return v;
}
