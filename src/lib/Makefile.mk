SUBDIRS-y :=
EXTDIRS-y :=

libobfu9 := libobfu9$(STATIC_LIB_EXT)

STATICLIBS-y := $(libobfu9)

OBJS-$(libobfu9)-y := \
	hash.c.o \
	hkey.c.o \
	md5.c.o \
	misc.c.o \
	opt.c.o \
	prng.c.o \
	recover_hkey.c.o

CFLAGS-y  += -I$(MKS_PROJDIR)/include -I$(MKS_PROJDIR)/libs/$(OS)/include/any
LDFLAGS-y += -L$(MKS_PROJDIR)/libs/$(OS)/x86 -L$(MKS_PROJDIR)/libs/$(OS)/x86_64
LDFLAGS-y += -lcrypto

CFLAGS-$(CONFIG_OS_WINDOWS) += -D__USE_MINGW_ANSI_STDIO=1
