/*
 * Obfu9
 * Copyright (C) 2020-2022 Guillaume Charifi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <ctype.h>
#include <errno.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lib/recover_hkey.h"
#include "lib/hash.h"
#include "lib/macro.h"
#include "lib/opt.h"


#define SPACES " \t"

static const char *stage_map[] = {
	[OBFU9_RECOVER_HKEY_STAGE_LOAD] = "load",
	[OBFU9_RECOVER_HKEY_STAGE_EXTRACT_1_4] = "extract_1_4",
	[OBFU9_RECOVER_HKEY_STAGE_EXTRACT_ABOVE_4] = "extract_above_4",
	[OBFU9_RECOVER_HKEY_STAGE_BRUTEFORCE] = "bruteforce",
	[OBFU9_RECOVER_HKEY_STAGE_MAX] = "*"
};

static const char *sub_stage_map[] = {
	[OBFU9_RECOVER_HKEY_SUB_STAGE_SCHEDULER] = "scheduler",
	[OBFU9_RECOVER_HKEY_SUB_STAGE_MAIN] = "main",
	[OBFU9_RECOVER_HKEY_SUB_STAGE_SHUFFLE] = "shuffle",
	[OBFU9_RECOVER_HKEY_SUB_STAGE_MAX] = "*"
};

static const char *step_map[] = {
	[OBFU9_RECOVER_HKEY_STEP_KN_ABOVE_7F] = "kn_above_7f",
	[OBFU9_RECOVER_HKEY_STEP_OFFSET] = "offset",
	[OBFU9_RECOVER_HKEY_STEP_MAIN] = "main",
	[OBFU9_RECOVER_HKEY_STEP_COUNT_MISSING_ENTRIES] = "count_missing_entries",
	[OBFU9_RECOVER_HKEY_STEP_KXK_FROM_PXK] = "kxk_from_pxk",
	[OBFU9_RECOVER_HKEY_STEP_KXK_FROM_KXK] = "kxk_from_kxk",
	[OBFU9_RECOVER_HKEY_STEP_PXK_FROM_PXK_X_KXK] = "pxk_from_pxk_x_kxk",
	[OBFU9_RECOVER_HKEY_STEP_PXP_FROM_PXK] = "pxp_from_pxk",
	[OBFU9_RECOVER_HKEY_STEP_LOG_COMPLETION] = "log_completion",
	[OBFU9_RECOVER_HKEY_STEP_CHECK_COMPLETE_KXK] = "check_complete_kxk",
	[OBFU9_RECOVER_HKEY_STEP_MAX] = "*"
};

static const char *log_lvl_map[] = {
	[OBFU9_RECOVER_HKEY_LOG_LEVEL_SILENT] = "silent",
	[OBFU9_RECOVER_HKEY_LOG_LEVEL_CRITICAL] = "critical",
	[OBFU9_RECOVER_HKEY_LOG_LEVEL_ERROR] = "error",
	[OBFU9_RECOVER_HKEY_LOG_LEVEL_WARN] = "warn",
	[OBFU9_RECOVER_HKEY_LOG_LEVEL_INFO] = "info",
	[OBFU9_RECOVER_HKEY_LOG_LEVEL_DEBUG] = "debug",
	[OBFU9_RECOVER_HKEY_LOG_LEVEL_TRACE] = "trace"
};

static int populate_log_lvls_from_str_helper(const char **str, const char **map, size_t array_size, size_t *idx)
{
	const char *s = *str;
	int result;
	size_t len;
	size_t i;

	len = strcspn(s, ":=,");
	for (i = 0; i < array_size; i++)
	{
		size_t l = strlen(map[i]);
		if (l != len)
			continue;

		result = strncmp(s, map[i], len);
		if (result == 0)
			break;
	}

	if (i >= array_size)
		return -1;

	*idx = i;
	*str = &s[len];
	return 0;
}

static int populate_log_lvls_from_str(struct obfu9_recover_hkey_context *ctx, const char *str)
{
	static const struct
	{
		const char **map;
		size_t map_size;
	} acts[] = {
		{ stage_map,     OBFU9_ARRAY_SIZE(stage_map) },
		{ sub_stage_map, OBFU9_ARRAY_SIZE(sub_stage_map) },
		{ step_map,      OBFU9_ARRAY_SIZE(step_map) }
	};
	int result;

	if (!*str)
		return 0;

	while (1)
	{
		size_t idxs[] = { OBFU9_RECOVER_HKEY_STAGE_MAX, OBFU9_RECOVER_HKEY_SUB_STAGE_MAX, OBFU9_RECOVER_HKEY_STEP_MAX };
		size_t stage_start = 0;
		size_t stage_end = OBFU9_RECOVER_HKEY_STAGE_MAX - 1;
		size_t sub_stage_start = 0;
		size_t sub_stage_end = OBFU9_RECOVER_HKEY_SUB_STAGE_MAX - 1;
		size_t step_start = 0;
		size_t step_end = OBFU9_RECOVER_HKEY_STEP_MAX - 1;
		size_t log_lvl;

		for (size_t i = 0; i < OBFU9_ARRAY_SIZE(acts); i++)
		{
			if (*str == ':' && i > 0)
				str++;
			result = populate_log_lvls_from_str_helper(&str, acts[i].map, acts[i].map_size, &idxs[i]);
			if (result < 0)
				return result;
			if (!*str || *str == '=')
				break;
		}

		if (*str != '=')
			return -1;
		str++;

		result = populate_log_lvls_from_str_helper(&str, log_lvl_map, OBFU9_ARRAY_SIZE(log_lvl_map), &log_lvl);
		if (result < 0)
			return result;

		if (idxs[0] < OBFU9_RECOVER_HKEY_STAGE_MAX)
			stage_start = stage_end = idxs[0];
		if (idxs[1] < OBFU9_RECOVER_HKEY_SUB_STAGE_MAX)
			sub_stage_start = sub_stage_end = idxs[1];
		if (idxs[2] < OBFU9_RECOVER_HKEY_STEP_MAX)
			step_start = step_end = idxs[2];

		for (size_t i = stage_start; i <= stage_end; i++)
			for (size_t j = sub_stage_start; j <= sub_stage_end; j++)
				for (size_t k = step_start; k <= step_end; k++)
					ctx->log_lvls[i][j][k] = (enum obfu9_recover_hkey_log_level)log_lvl;

		if (!*str)
			break;

		if (*str != ',')
			return -1;
		str++;
	}

	return 0;
}

static int packed_hash_from_escaped_str(obfu9_hash_packed_t *h, const char *str)
{
	char hbuf[OBFU9_HASH_SIZE];
	size_t len;
	size_t hbuflen;
	int last_was_escape;

	len = strlen(str);
	hbuflen = 0;
	last_was_escape = 0;

	for (size_t i = 0; i < len; i++)
	{
		char c = str[i];

		if (hbuflen == OBFU9_HASH_SIZE - 1)
			return -1;

		if (last_was_escape)
		{
			switch (c)
			{
				case 'b': c = '\b'; break;
				case 't': c = '\t'; break;
				case 'n': c = '\n'; break;
				case 'f': c = '\f'; break;
				case 'r': c = '\r'; break;
				case '#': c = '#'; break;
				case '\\': c = '\\'; break;
				case 'x':
				{
					char b[3];
					char *endp;

					if (i + 3 > len)
						return -1;

					b[0] = str[++i];
					b[1] = str[++i];
					b[2] = '\0';

					c = strtol(b, &endp, 16);
					if (endp != &b[2])
						return -1;
					break;
				}
				default:
					return -1;
			}

			last_was_escape = 0;
		}
		else if (c == '\\')
		{
			last_was_escape = 1;
			continue;
		}

		hbuf[hbuflen++] = c;
	}

	*h = obfu9_hash_pack_len(hbuf, hbuflen);
	return 0;
}

struct log_cb_ctx
{
	int first;
	enum obfu9_recover_hkey_stage stage;
	enum obfu9_recover_hkey_sub_stage sub_stage;
	enum obfu9_recover_hkey_step step;
};

static void log_cb(void *priv, struct obfu9_recover_hkey_context *ctx, enum obfu9_recover_hkey_log_level lvl, const char *format, ...)
{
	struct log_cb_ctx *cb_ctx = (struct log_cb_ctx *)priv;
	va_list ap;
	const char *spacer = "";
	char log_lvl_c;

	if (!cb_ctx->first)
	{
		if (cb_ctx->stage != ctx->stage
				|| cb_ctx->sub_stage != ctx->sub_stage)
			spacer = "\n";
	}

	cb_ctx->first = 0;
	cb_ctx->stage = ctx->stage;
	cb_ctx->sub_stage = ctx->sub_stage;
	cb_ctx->step = ctx->step;

	log_lvl_c = log_lvl_map[lvl][0];
	if (lvl == OBFU9_RECOVER_HKEY_LOG_LEVEL_SILENT)
		log_lvl_c = '-';

	fprintf(stderr, "%s%c: %s:%s:%s: ", spacer, toupper(log_lvl_c), stage_map[ctx->stage], sub_stage_map[ctx->sub_stage], step_map[ctx->step]);

	va_start(ap, format);
	vfprintf(stderr, format, ap);
	va_end(ap);
}

static int print_usage(int argc, char **argv)
{
	const char *argv0 = argc >= 1 ? argv[0] : "deobfu9_hkey";

	fprintf(stderr, "Usage:\n"
			"    %s [-log <stage>[:<sub_stage>[:<step>]]=<level>[,...]] [-i <input_file>] [-o <output_file>]\n"
			"\n"
			"Read either:\n"
			" - from stdin\n"
			" - from the specified <input_file>\n"
			"\n"
			"Output the hkey either:\n"
			" - to stdout\n"
			" - to the specified <output_file>\n"
			"\n"
			"Log specifier format:\n"
			" Default options: *=trace,*:shuffle=info\n"
			"\n"
			" <stage>:\n"
			"  - load\n"
			"  - extract_1_4\n"
			"  - extract_above_4\n"
			"  - bruteforce\n"
			"  - *\n"
			"\n"
			" <sub_stage>:\n"
			"  - scheduler\n"
			"  - main\n"
			"  - shuffle\n"
			"  - *\n"
			"\n"
			" <step>:\n"
			"  - kn_above_7f\n"
			"  - offset\n"
			"  - main\n"
			"  - count_missing_entries\n"
			"  - kxk_from_pxk\n"
			"  - kxk_from_kxk\n"
			"  - pxk_from_pxk_x_kxk\n"
			"  - pxp_from_pxk\n"
			"  - log_completion\n"
			"  - check_complete_kxk\n"
			"  - *\n"
			"\n"
			" <level>:\n"
			"  - silent\n"
			"  - critical\n"
			"  - error\n"
			"  - warn\n"
			"  - info\n"
			"  - debug\n"
			"  - trace\n"
			"\n"
			"Input file example:\n"
			"\n"
			"# This is a comment.\n"
			"# This section should be the first, containing identifiers of increasing size from 1 to 4.\n"
			"# It is treated in a single pass.\n"
			"[extract_1_4]\n"
			"\n"
			"a @\\x01 # A comment\n"
			"b r\\x01\n"
			"d t\\x01\n"
			"as A\\x0e\\x01\n"
			"id \\x1d\\x0b\\x01\n"
			"Num \\#Fx\\x01\n"
			"Ray \\x1b\\x0by\\x01\n"
			"Std \\\\0F\\x01\n"
			"haxe x+[\\x1f\\x01\n"
			"head \\x0e\\x1aE^\\x01\n"
			"init \\x1c[Dl\\x01\n"
			"# The shuffle instruction is required to combine the information extracted from several identifiers.\n"
			"# Exactly one should be placed at the end of this section.\n"
			"---shuffle---\n"
			"\n"
			"# This section should contain at least one identifier per size increasing from 5 to 16.\n"
			"# It is treated in multiple passes, as long as a change occur.\n"
			"[extract_above_4]\n"
			"\n"
			"Bytes \\x02\\by<\\x03\n"
			"CData G}+n\n"
			"# There should be a shuffle instruction at the end of a group of the same size.\n"
			"---shuffle---\n"
			"\n"
			"BASE64 eH@\\x20\\x02\n"
			"Custom v\\bT8\\x03\n"
			"---shuffle---\n"
			"\n"
			"Dynamic 5\\x03\\x1e'\\x03\n"
			"---shuffle---\n"
			"\n"
			"# [...]\n"
			"\n"
			"skip_constructor \\x1c\\x14'\\x01\n"
			"---shuffle---\n"
			"\n"
			"# The last section does the actual brute force on the previously extracted data to find the key.\n"
			"# It should contain two or more entries to ensure that the correct key is found.\n"
			"[bruteforce]\n"
			"\n"
			"a @\\x01\n"
			"b r\\x01\n"
			"skip_constructor \\x1c\\x14'\\x01\n"
			"# End of file\n"
			"\n"
			"More examples can be found either in the sources or in the distributed compiled package.\n",
			argv0);
	return 1;
}

enum opt_id
{
	OPT_I = 0,
	OPT_LOG,
	OPT_O,
	OPT__END_
};

int main(int argc, char **argv)
{
	int status = 1;
	int result;
	struct obfu9_opt opt_tpl[] = {
		OBFU9_OPT_STR(OPT_I,   "-i"),
		OBFU9_OPT_STR(OPT_LOG, "-log"),
		OBFU9_OPT_STR(OPT_O,   "-o"),
		OBFU9_OPT_END(OPT__END_)
	};
	struct obfu9_opt *opts;
	struct obfu9_recover_hkey_context ctx;
	struct log_cb_ctx cb_ctx;
	enum obfu9_recover_hkey_errno deobfu_result;
	const char *in_filename;
	const char *log_options;
	const char *out_filename;
	FILE *in_fp = stdin;
	FILE *out_fp = stdout;
	const char *hkey;

	/*** Handle options ***/

	opts = obfu9_opt_parse(argc, argv, opt_tpl);
	if (!opts)
		return print_usage(argc, argv);

	in_filename  = obfu9_opt_get_str_or(opts, OPT_I, NULL);
	log_options  = obfu9_opt_get_str_or(opts, OPT_LOG, "*=trace,*:shuffle=info");
	out_filename = obfu9_opt_get_str_or(opts, OPT_O, NULL);

	obfu9_recover_hkey_context_init(&ctx);
	cb_ctx.first = 1;
	cb_ctx.stage = ctx.stage;
	cb_ctx.sub_stage = ctx.sub_stage;
	cb_ctx.step = ctx.step;
	obfu9_recover_hkey_set_log_callback(&ctx, log_cb, &cb_ctx);

	result = populate_log_lvls_from_str(&ctx, log_options);
	if (result < 0)
		return print_usage(argc, argv);

	/*** Open files ***/

	if (in_filename)
	{
#ifdef EISDIR
		errno = 0;
		in_fp = fopen(in_filename, "r+");
		if (in_fp)
			fclose(in_fp);

		if (errno != EISDIR)
		{
#endif
			errno = 0;
			in_fp = fopen(in_filename, "r");
#ifdef EISDIR
		}
#endif

		if (!in_fp)
		{
			fprintf(stderr, "*ERR*: Failed to open the input file \"%s\": %s\n", in_filename, strerror(errno));
			goto fail_open_in_fp;
		}
	}

	if (out_filename)
	{
		errno = 0;
		out_fp = fopen(out_filename, "w");
		if (!out_fp)
		{
			fprintf(stderr, "*ERR*: Failed to open the output file \"%s\": %s\n", out_filename, strerror(errno));
			goto fail_open_out_fp;
		}
	}

	/*** Load input file ***/

	while (!feof(in_fp) && !ferror(in_fp))
	{
		static char line_buf[1024];
		size_t lineno = 0;
		char *l;
		size_t len;
		char *identifier;
		char *hstr;
		obfu9_hash_packed_t h = 0;

		l = fgets(line_buf, sizeof(line_buf), in_fp);
		if (!l)
			continue;

		lineno++;

		/* Remove the trailing '\n' */
		l[strcspn(l, "\n")] = '\0';

		/* Ignore comments and blank lines */
		l = &l[strspn(l, SPACES)];
		if (!*l || *l == '#')
			continue;

		identifier = l;
		len = strcspn(identifier, SPACES);
		if (!len)
		{
			fprintf(stderr, "<input>:%zu: Identifier not found (length == 0): \"%s\"\n", lineno, identifier);
			goto fail_load_in;
		}

		l = &identifier[len];
		if (*l)
			l++;
		identifier[len] = '\0';

		hstr = NULL;

		l = &l[strspn(l, SPACES)];
		if (*l && *l != '#')
		{
			hstr = l;
			len = strcspn(hstr, SPACES);
			if (!len)
			{
				fprintf(stderr, "<input>:%zu: Hash not found (length == 0): \"%s\"\n", lineno, hstr);
				goto fail_load_in;
			}

			l = &hstr[len];
			if (*l)
				l++;
			hstr[len] = '\0';

			l = &l[strspn(l, SPACES)];
			if (*l && *l != '#')
			{
				fprintf(stderr, "<input>:%zu: Unexpected characters at the end of the line after the hash: \"%s\"\n", lineno, l);
				goto fail_load_in;
			}
		}

		if (!hstr)
		{
			result = strcmp(identifier, "[extract_1_4]");
			if (result == 0)
			{
				obfu9_recover_hkey_append_marker_extract_1_4(&ctx);
				continue;
			}

			result = strcmp(identifier, "[extract_above_4]");
			if (result == 0)
			{
				obfu9_recover_hkey_append_marker_extract_above_4(&ctx);
				continue;
			}

			result = strcmp(identifier, "[bruteforce]");
			if (result == 0)
			{
				obfu9_recover_hkey_append_marker_bruteforce(&ctx);
				continue;
			}

			result = strcmp(identifier, "---shuffle---");
			if (result == 0)
			{
				obfu9_recover_hkey_append_marker_shuffle(&ctx);
				continue;
			}

			fprintf(stderr, "<input>:%zu: Unrecognized directive: \"%s\"\n", lineno, identifier);
			goto fail_load_in;
		}

		result = packed_hash_from_escaped_str(&h, hstr);
		if (result < 0)
		{
			fprintf(stderr, "<input>:%zu: Failed to convert hash from string to packed: \"%s\"\n", lineno, hstr);
			goto fail_load_in;
		}

		obfu9_recover_hkey_append_match(&ctx, identifier, h);
	}

	if (ferror(in_fp))
	{
		fprintf(stderr, "Error while reading the input file\n");
		goto fail_load_in;
	}

	/*** Run ***/

	deobfu_result = obfu9_recover_hkey_run(&ctx);
	if (deobfu_result != OBFU9_RECOVER_HKEY_ESUCCESS)
	{
		fprintf(stderr, "Deobfuscation pipeline failed\n");
		goto fail_run;
	}

	/*** Output the hkey ***/

	hkey = ctx.hkey;
	fprintf(out_fp, "%.2hhx%.2hhx%.2hhx%.2hhx%.2hhx%.2hhx%.2hhx%.2hhx"
			"%.2hhx%.2hhx%.2hhx%.2hhx%.2hhx%.2hhx%.2hhx%.2hhx\n",
			hkey[0], hkey[1], hkey[2],  hkey[3],  hkey[4],  hkey[5],  hkey[6],  hkey[7],
			hkey[8], hkey[9], hkey[10], hkey[11], hkey[12], hkey[13], hkey[14], hkey[15]);

	status = 0;

fail_run:
fail_load_in:
	if (out_filename)
		fclose(out_fp);

fail_open_out_fp:
	if (in_filename)
		fclose(in_fp);

fail_open_in_fp:
	obfu9_recover_hkey_context_reset(&ctx);
	return status;
}
